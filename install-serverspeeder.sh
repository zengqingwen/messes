#!/usr/bin/env bash

# 执行/etc/rc.d/rc.local中的开机自启动脚本，默认执行路径是根目录（pwd =》 /）
if [ -f "/root/serverspeeder.sh" ]; then
    echo "【info】serverspeeder has installed, status it..." >> /root/trac.log
    echo "【start server】启动机器@`date`" >> /root/trac.log
    # serverSpeeder安装完后，就已经设置成开机自启动了，所以不需要再在下面的 start/restart 这行了
    # echo "【log】`service serverSpeeder restart`" >> /root/trac.log
    echo "【log】`service serverSpeeder status`" >> /root/trac.log
else
    echo "【info】serverspeeder has not installed, install it AND start it..." >> /root/trac.log
    echo "【start server】启动机器@`date`" >> /root/trac.log
    wget -N --no-check-certificate -O /root/serverspeeder.sh https://gitlab.com/zengqingwen/messes/raw/public/serverspeeder.sh
    bash /root/serverspeeder.sh
    echo "【log】`service serverSpeeder status`" >> /root/trac.log
fi
